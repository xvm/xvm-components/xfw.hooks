# Changelog

## v11.1.1

* fix crash with old MSVC Redistributable

## v11.1.0

* improve thread safety

## v11.0.0

* MT 1.30.0 support

## v10.0.1

* fix includes

## v10.0.0

* first version
