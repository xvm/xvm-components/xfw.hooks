// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

#pragma once

// stdlib
#include <functional>
#include <map>
#include <vector>

// Windows
#include <Windows.h>

// Direct3D
#include <d3d11.h>
#include <dxgi.h>

// xfw.hooks
#include "xfw_native_hooks.h"
#include "xfw_native_hooks_macro.h"



namespace XFW::Native::Hooks {

    //
    // Enums
    //

    enum class IDXGISwapChain_methods {
        // IUnknown
        QueryInterface = 0,
        AddRef = 1,
        Release = 2,

        // IDXGIObject
        SetPrivateData = 3,
        SetPrivateDataInterface = 4,
        GetPrivateData = 5,
        GetParent = 6,

        // IDXGIDeviceSubObject
        GetDevice = 7,

        // IDXGISwapChain
        Present = 8,
        GetBuffer = 9,
        SetFullscreenState = 10,
        GetFullscreenState = 11,
        GetDesc = 12,
        ResizeBuffers = 13,
        ResizeTarget = 14,
        GetContainingOutput = 15,
        GetFrameStatistics = 16,
        GetLastPresentCount = 17,

        Last = 17
    };


    //
    // Hook Manager
    //

    class XFW_NATIVE_HOOKS_EXPORT HookmanagerD3D {

        // Init
      public:
        static HookmanagerD3D &instance();
        bool init();
        bool inited() const;

      private:
        HookmanagerD3D() = default;
        ~HookmanagerD3D() = default;

      private:
        bool _inited = false;


        // IDXGISwapChain
      public:
        XFW_HOOKS_REG_DECLARATION(IDXGISwapChain, Present, UINT, UINT);
        XFW_HOOKS_REG_DECLARATION(IDXGISwapChain, ResizeBuffers, UINT, UINT, UINT, DXGI_FORMAT, UINT);

      private:
        XFW_HOOKS_FUN_DECLARATION(IDXGISwapChain, Present, UINT, UINT);
        XFW_HOOKS_FUN_DECLARATION(IDXGISwapChain, ResizeBuffers, UINT, UINT, UINT, DXGI_FORMAT, UINT);

      private:
        XFW_HOOKS_VTABLE_DECLARATION(IDXGISwapChain);
    };
} // namespace XFW::Native::Hooks
