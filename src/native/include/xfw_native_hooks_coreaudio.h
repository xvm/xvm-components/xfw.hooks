// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

#pragma once

// stdlib
#include <functional>
#include <map>
#include <vector>

// Core Audio
#include <Audioclient.h>

// xfw.hooks
#include "xfw_native_hooks.h"
#include "xfw_native_hooks_macro.h"



namespace XFW::Native::Hooks {

    //
    // Enums
    //
    enum class IAudioClient_methods {
        // IUnknown
        QueryInterface = 0,
        AddRef = 1,
        Release = 2,

        // IAudioClient
        Initialize = 3,
        GetBufferSize = 4,
        GetStreamLatency = 5,
        GetCurrentPadding = 6,
        IsFormatSupported = 7,
        GetMixFormat = 8,
        GetDevicePeriod = 9,
        Start = 10,
        Stop = 11,
        Reset = 12,
        SetEventHandle = 13,
        GetService = 14,

        Last = 14
    };

    enum class IAudioRenderClient_methods {
        // IUnknown
        QueryInterface = 0,
        AddRef = 1,
        Release = 2,

        // IAudioRenderClient
        GetBuffer = 3,
        ReleaseBuffer = 4,

        Last = 4
    };
    //
    // Hook Manager
    //

    class XFW_NATIVE_HOOKS_EXPORT HookmanagerCoreaudio {

        //
        // Init
        //
      public:
        static HookmanagerCoreaudio &instance();

        bool init();
        bool inited() const;

      private:
        HookmanagerCoreaudio() = default;

        ~HookmanagerCoreaudio() = default;

      private:
        bool _inited = false;

        //
        // IAudioClient
        //
      public:
        XFW_HOOKS_REG_DECLARATION(IAudioClient, Initialize  , AUDCLNT_SHAREMODE, DWORD, REFERENCE_TIME, REFERENCE_TIME, const WAVEFORMATEX *, LPCGUID);
        XFW_HOOKS_REG_DECLARATION(IAudioClient, GetMixFormat, WAVEFORMATEX **);
        XFW_HOOKS_REG_DECLARATION(IAudioClient, Start);
        XFW_HOOKS_REG_DECLARATION(IAudioClient, Stop);
        XFW_HOOKS_REG_DECLARATION(IAudioClient, Reset);
        XFW_HOOKS_REG_DECLARATION(IAudioClient, GetService, REFIID, void **);

      private:
        XFW_HOOKS_FUN_DECLARATION(IAudioClient, Initialize, AUDCLNT_SHAREMODE, DWORD, REFERENCE_TIME, REFERENCE_TIME, const WAVEFORMATEX *,LPCGUID);
        XFW_HOOKS_FUN_DECLARATION(IAudioClient, GetMixFormat, WAVEFORMATEX **);
        XFW_HOOKS_FUN_DECLARATION(IAudioClient, Start);
        XFW_HOOKS_FUN_DECLARATION(IAudioClient, Stop);
        XFW_HOOKS_FUN_DECLARATION(IAudioClient, Reset);
        XFW_HOOKS_FUN_DECLARATION(IAudioClient, GetService, REFIID, void **);

      private:
        XFW_HOOKS_VTABLE_DECLARATION(IAudioClient);



        //
        // IAudioRenderClient
        //
      public:
        XFW_HOOKS_REG_DECLARATION(IAudioRenderClient, GetBuffer, UINT32, BYTE **);
        XFW_HOOKS_REG_DECLARATION(IAudioRenderClient, ReleaseBuffer, UINT32, DWORD);

      private:
        XFW_HOOKS_FUN_DECLARATION(IAudioRenderClient, GetBuffer, UINT32, BYTE **);
        XFW_HOOKS_FUN_DECLARATION(IAudioRenderClient, ReleaseBuffer, UINT32, DWORD);

      private:
        XFW_HOOKS_VTABLE_DECLARATION(IAudioRenderClient);
    };
} // namespace XFW::Native::Hooks
