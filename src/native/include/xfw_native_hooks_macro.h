#pragma once

//
// Includes
//

#include <functional>
#include <map>
#include <mutex>
#include <string>
#include <utility>



//
// Escaping
//

#define XFW_HOOKS_COMMA ,


//
// Concate
//

#define XFW_HOOKS_CONCATE_(X, Y) X##Y
#define XFW_HOOKS_CONCATE(X, Y) XFW_HOOKS_CONCATE_(X, Y)


//
// VA_MACRO
//

#define XFW_HOOKS_MSVC(MACRO, ARGS) MACRO ARGS
#define XFW_HOOKS_NUM_ARGS_2(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, TOTAL, ...) TOTAL
#define XFW_HOOKS_NUM_ARGS_1(...) XFW_HOOKS_MSVC(XFW_HOOKS_NUM_ARGS_2, (__VA_ARGS__))
#define XFW_HOOKS_NUM_ARGS(...) XFW_HOOKS_NUM_ARGS_1(__VA_ARGS__, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
#define XFW_HOOKS_VA_MACRO(MACRO, ...) XFW_HOOKS_MSVC(XFW_HOOKS_CONCATE, (MACRO, XFW_HOOKS_NUM_ARGS(__VA_ARGS__)))(__VA_ARGS__)


//
// Vtable
//

#define XFW_HOOKS_VTABLE_DECLARATION(cls) std::vector<void *> _##cls##_vtable;



//
// Registration
//

#define XFW_HOOKS_REG_DECLARATION(cls, method, ...)                                                                                        \
    bool cls##_##method##_register(const std::string& id, HookPlace place, std::function<void(cls *, __VA_ARGS__)> func);                  \
    bool cls##_##method##_unregister(const std::string &id, HookPlace place);                                                              \


#define XFW_HOOKS_REG_DEFINITION(hookmanager, cls, method, ...)                                                                            \
    bool hookmanager::cls##_##method##_register(const std::string &id, HookPlace place, std::function<void(cls *, __VA_ARGS__)> func) {    \
        if (!_##cls##_##method##_original) {                                                                                               \
            void *address_target = FunctionAddressFixup(_##cls##_vtable[static_cast<size_t>(cls##_methods::method)]);                      \
            void *address_hook = reinterpret_cast<void *>(cls##_##method);                                                                 \
            void *address_original = nullptr;                                                                                              \
            if (!address_target || !address_hook) {                                                                                        \
                return false;                                                                                                              \
            }                                                                                                                              \
            if (!HookCreate(address_target, address_hook, &address_original)) {                                                            \
                return false;                                                                                                              \
            }                                                                                                                              \
            if (!HookEnable(address_target)) {                                                                                             \
                return false;                                                                                                              \
            }                                                                                                                              \
            _##cls##_##method##_original = address_original;                                                                               \
        }                                                                                                                                  \
        {                                                                                                                                  \
            std::scoped_lock lock(_##cls##_##method##_mutex);                                                                              \
            if (place == HookPlace::After) {                                                                                               \
                _##cls##_##method##_hooks_after.emplace_back(id, std::move(func));                                                         \
            } else {                                                                                                                       \
                _##cls##_##method##_hooks_before.emplace_back(id, std::move(func));                                                        \
            }                                                                                                                              \
        }                                                                                                                                  \
        return true;                                                                                                                       \
    }                                                                                                                                      \
    bool hookmanager::cls##_##method##_unregister(const std::string &id, HookPlace place) {                                                \
        bool result = false;                                                                                                               \
        std::scoped_lock lock(_##cls##_##method##_mutex);                                                                                  \
        if (place == HookPlace::After) {                                                                                                   \
            result = std::erase_if(_##cls##_##method##_hooks_after, [&id](_##cls##_##method##_struct &x) { return x.first == id; }) > 0U;  \
        } else {                                                                                                                           \
            result = std::erase_if(_##cls##_##method##_hooks_before, [&id](_##cls##_##method##_struct &x) { return x.first == id; }) > 0U; \
        }                                                                                                                                  \
        return result;                                                                                                                     \
    }



//
// Function
//

#define XFW_HOOKS_FUN_DECLARATION(cls, method, ...)                                                                                        \
    typedef std::pair<std::string, std::function<void(cls *, __VA_ARGS__)>>  _##cls##_##method##_struct;                                   \
    static HRESULT __stdcall cls##_##method(cls *pClass, __VA_ARGS__);                                                                     \
    std::vector< _##cls##_##method##_struct > _##cls##_##method##_hooks_before{};                                                          \
    std::vector< _##cls##_##method##_struct > _##cls##_##method##_hooks_after{};                                                           \
    void *_##cls##_##method##_original{};                                                                                                  \
    std::mutex _##cls##_##method##_mutex{};

#define XFW_HOOKS_FUN_DEFINITION_NOARGS_(hookmanager, cls, method)                                                                         \
    HRESULT __stdcall hookmanager::cls##_##method(cls *pClass) {                                                                           \
        HRESULT result;                                                                                                                    \
        auto &mgr = hookmanager::instance();                                                                                               \
        {                                                                                                                                  \
            std::scoped_lock lock(mgr._##cls##_##method##_mutex);                                                                          \
            for (auto &[key, function] : mgr._##cls##_##method##_hooks_before) {                                                           \
                function(pClass);                                                                                                          \
            }                                                                                                                              \
            result = reinterpret_cast<cls##_##method##_typedef>(mgr._##cls##_##method##_original)(pClass);                                 \
            for (auto &[key, function] : mgr._##cls##_##method##_hooks_after) {                                                            \
                function(pClass);                                                                                                          \
            }                                                                                                                              \
        }                                                                                                                                  \
        return result;                                                                                                                     \
    }

#define XFW_HOOKS_FUN_DEFINITION_(hookmanager, cls, method, args, typeargs)                                                                \
    HRESULT __stdcall hookmanager::cls##_##method(cls *pClass, typeargs) {                                                                 \
        HRESULT result;                                                                                                                    \
        auto &mgr = hookmanager::instance();                                                                                               \
        {                                                                                                                                  \
            std::scoped_lock lock(mgr._##cls##_##method##_mutex);                                                                          \
            for (auto &[key,function] : mgr._##cls##_##method##_hooks_before) {                                                            \
                function(pClass, args);                                                                                                    \
            }                                                                                                                              \
            result = reinterpret_cast<cls##_##method##_typedef>(mgr._##cls##_##method##_original)(pClass, args);                           \
            for (auto &[key, function] : mgr._##cls##_##method##_hooks_after) {                                                            \
                function(pClass, args);                                                                                                    \
            }                                                                                                                              \
        }                                                                                                                                  \
        return result;                                                                                                                     \
    }

#define XFW_HOOKS_FUN_DEFINITION_3(hookmanager, cls, method) XFW_HOOKS_FUN_DEFINITION_NOARGS_(hookmanager, cls, method);

#define XFW_HOOKS_FUN_DEFINITION_4(hookmanager, cls, method, t1) XFW_HOOKS_FUN_DEFINITION_(hookmanager, cls, method, a1, t1 a1);

#define XFW_HOOKS_FUN_DEFINITION_5(hookmanager, cls, method, t1, t2)                                                                       \
    XFW_HOOKS_FUN_DEFINITION_(hookmanager, cls, method, a1 XFW_HOOKS_COMMA a2, t1 a1 XFW_HOOKS_COMMA t2 a2);

#define XFW_HOOKS_FUN_DEFINITION_6(hookmanager, cls, method, t1, t2, t3)                                                                   \
    XFW_HOOKS_FUN_DEFINITION_(hookmanager, cls, method, a1 XFW_HOOKS_COMMA a2 XFW_HOOKS_COMMA a3,                                          \
                              t1 a1 XFW_HOOKS_COMMA t2 a2 XFW_HOOKS_COMMA t3 a3);

#define XFW_HOOKS_FUN_DEFINITION_7(hookmanager, cls, method, t1, t2, t3, t4)                                                               \
    XFW_HOOKS_FUN_DEFINITION_(hookmanager,cls,method,a1 XFW_HOOKS_COMMA a2 XFW_HOOKS_COMMA a3 XFW_HOOKS_COMMA a4, t1 a1 XFW_HOOKS_COMMA t2 a2 XFW_HOOKS_COMMA t3 a3 XFW_HOOKS_COMMA t4 a4));

#define XFW_HOOKS_FUN_DEFINITION_8(hookmanager, cls, method, t1, t2, t3, t4, t5)                                                           \
    XFW_HOOKS_FUN_DEFINITION_(hookmanager, cls, method, a1 XFW_HOOKS_COMMA a2 XFW_HOOKS_COMMA a3 XFW_HOOKS_COMMA a4 XFW_HOOKS_COMMA a5,    \
                              t1 a1 XFW_HOOKS_COMMA t2 a2 XFW_HOOKS_COMMA t3 a3 XFW_HOOKS_COMMA t4 a4 XFW_HOOKS_COMMA t5 a5);

#define XFW_HOOKS_FUN_DEFINITION_9(hookmanager, cls, method, t1, t2, t3, t4, t5, t6)                                                       \
    XFW_HOOKS_FUN_DEFINITION_(                                                                                                             \
        hookmanager, cls, method, a1 XFW_HOOKS_COMMA a2 XFW_HOOKS_COMMA a3 XFW_HOOKS_COMMA a4 XFW_HOOKS_COMMA a5 XFW_HOOKS_COMMA a6,       \
        t1 a1 XFW_HOOKS_COMMA t2 a2 XFW_HOOKS_COMMA t3 a3 XFW_HOOKS_COMMA t4 a4 XFW_HOOKS_COMMA t5 a5 XFW_HOOKS_COMMA t6 a6);

#define XFW_HOOKS_FUN_DEFINITION(...) XFW_HOOKS_VA_MACRO(XFW_HOOKS_FUN_DEFINITION_, __VA_ARGS__)


//
// Typedef
//

#define XFW_HOOKS_TYPEDEF_DECLARATION(cls, method, ...)                                                                                    \
    typedef HRESULT(STDMETHODCALLTYPE *cls##_##method##_typedef)(cls * pClass, __VA_ARGS__);\
