// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

#pragma once

// stdlib
#include <cstdint>

// xfw.hooks
#include "xfw_native_hooks_export.h"

namespace XFW::Native::Hooks {

    //
    // Enums
    //

    /// <summary>
    /// Hook placement
    /// </summary>
    enum class HookPlace {
        /// <summary>
        /// Execute hook before original function call
        /// </summary>
        Before = 0,

        /// <summary>
        /// Execute hook after original function call
        /// </summary>
        After = 1,
    };



    //
    // Functions
    //

    /// <summary>
    /// Resolve real function address in case of intermediate hooks
    /// </summary>
    /// <param name="address">function address</param>
    /// <returns>real function address</returns>
    XFW_NATIVE_HOOKS_EXPORT [[nodiscard]] void *FunctionAddressFixup(void *address);

    /// <summary>
    /// Read 32 bits from address
    /// </summary>
    /// <param name="address">address to read</param>
    /// <returns>address contents</returns>
    XFW_NATIVE_HOOKS_EXPORT [[nodiscard]] uint32_t MemoryRead32(size_t address);

    /// <summary>
    /// Read 64 bits from address
    /// </summary>
    /// <param name="address">address to read</param>
    /// <returns>address contents</returns>
    XFW_NATIVE_HOOKS_EXPORT [[nodiscard]] uint64_t MemoryRead64(size_t address);

    XFW_NATIVE_HOOKS_EXPORT [[nodiscard]] bool HookCreate(void *pTarget, void *pDetour, void **ppOriginal);

    XFW_NATIVE_HOOKS_EXPORT [[nodiscard]] bool HookRemove(void *pTarget);

    XFW_NATIVE_HOOKS_EXPORT [[nodiscard]] bool HookEnable(void *pTarget);

    XFW_NATIVE_HOOKS_EXPORT [[nodiscard]] bool HookDisable(void *pTarget);

    XFW_NATIVE_HOOKS_EXPORT [[nodiscard]] bool HookInitialize();

} // namespace XFW::Native::Hooks
