// SPDX-License-Identifier: MIT
// Copyright (c) 2012-2021 Chun-Ying Huang
// Copyright (c) 2022 Mikhail Paulyshka

// stdlib
#include <memory>

// Windows
#include <Windows.h>
#include <wrl.h>

// Core Audio
#include <Audioclient.h>
#include <mmdeviceapi.h>

// xfw.hooks
#include "xfw_native_hooks.h"
#include "xfw_native_hooks_coreaudio.h"


namespace XFW::Native::Hooks {
    //
    // Init
    //

    HookmanagerCoreaudio &HookmanagerCoreaudio::instance() {
        static HookmanagerCoreaudio instance;
        return instance;
    }

    bool HookmanagerCoreaudio::init() {
        if (!_inited) {
            Microsoft::WRL::ComPtr<IMMDeviceEnumerator> deviceEnumerator;
            Microsoft::WRL::ComPtr<IMMDevice> device;
            Microsoft::WRL::ComPtr<IAudioClient> audioClient;
            Microsoft::WRL::ComPtr<IAudioRenderClient> audioRenderClient;
            WAVEFORMATEX *waveFormat = nullptr;

            if (FAILED(CoCreateInstance(__uuidof(MMDeviceEnumerator), nullptr, CLSCTX_ALL, __uuidof(deviceEnumerator),
                                        reinterpret_cast<void **>(deviceEnumerator.GetAddressOf())))) {
                return false;
            }

            if (FAILED(deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, device.GetAddressOf()))) {
                return false;
            }

            if (FAILED(
                    device->Activate(__uuidof(audioClient), CLSCTX_ALL, nullptr, reinterpret_cast<void **>(audioClient.GetAddressOf())))) {
                return false;
            }

            if (FAILED(audioClient->GetMixFormat(&waveFormat))) {
                return false;
            }


            if (FAILED(audioClient->Initialize(AUDCLNT_SHAREMODE_SHARED, 0, 10000000, 0, waveFormat, nullptr))) {
                return false;
            }

            if (FAILED(audioClient->GetService(IID_PPV_ARGS(&audioRenderClient)))) {
                return false;
            }

            _IAudioClient_vtable.resize(static_cast<size_t>(IAudioClient_methods::Last) + 1);
            std::memcpy(_IAudioClient_vtable.data(), *reinterpret_cast<void **>(audioClient.Get()),
                        _IAudioClient_vtable.size() * sizeof(void *));

            _IAudioRenderClient_vtable.resize(static_cast<size_t>(IAudioRenderClient_methods::Last) + 1);
            std::memcpy(_IAudioRenderClient_vtable.data(), *reinterpret_cast<void **>(audioRenderClient.Get()),
                        _IAudioRenderClient_vtable.size() * sizeof(void *));


            _inited = true;
        }

        return true;
    }

    bool HookmanagerCoreaudio::inited() const { return _inited;
    }

    //
    // IAudioClient
    //

    XFW_HOOKS_TYPEDEF_DECLARATION(                 IAudioClient, Initialize, AUDCLNT_SHAREMODE, DWORD, REFERENCE_TIME, REFERENCE_TIME, const WAVEFORMATEX *, LPCGUID);
    XFW_HOOKS_REG_DEFINITION(HookmanagerCoreaudio, IAudioClient, Initialize, AUDCLNT_SHAREMODE, DWORD, REFERENCE_TIME, REFERENCE_TIME, const WAVEFORMATEX *, LPCGUID);
    XFW_HOOKS_FUN_DEFINITION(HookmanagerCoreaudio, IAudioClient, Initialize, AUDCLNT_SHAREMODE, DWORD, REFERENCE_TIME, REFERENCE_TIME, const WAVEFORMATEX *, LPCGUID);
 
    XFW_HOOKS_TYPEDEF_DECLARATION(                 IAudioClient, GetMixFormat, WAVEFORMATEX **);
    XFW_HOOKS_REG_DEFINITION(HookmanagerCoreaudio, IAudioClient, GetMixFormat, WAVEFORMATEX **);
    XFW_HOOKS_FUN_DEFINITION(HookmanagerCoreaudio, IAudioClient, GetMixFormat, WAVEFORMATEX **);

    XFW_HOOKS_TYPEDEF_DECLARATION(IAudioClient, Start);
    XFW_HOOKS_REG_DEFINITION(HookmanagerCoreaudio, IAudioClient, Start);
    XFW_HOOKS_FUN_DEFINITION(HookmanagerCoreaudio, IAudioClient, Start);

    XFW_HOOKS_TYPEDEF_DECLARATION(IAudioClient, Stop);
    XFW_HOOKS_REG_DEFINITION(HookmanagerCoreaudio, IAudioClient, Stop);
    XFW_HOOKS_FUN_DEFINITION(HookmanagerCoreaudio, IAudioClient, Stop);

    XFW_HOOKS_TYPEDEF_DECLARATION(IAudioClient, Reset);
    XFW_HOOKS_REG_DEFINITION(HookmanagerCoreaudio, IAudioClient, Reset);
    XFW_HOOKS_FUN_DEFINITION(HookmanagerCoreaudio, IAudioClient, Reset);

    XFW_HOOKS_TYPEDEF_DECLARATION(                 IAudioClient, GetService, REFIID, void **);
    XFW_HOOKS_REG_DEFINITION(HookmanagerCoreaudio, IAudioClient, GetService, REFIID, void **);
    XFW_HOOKS_FUN_DEFINITION(HookmanagerCoreaudio, IAudioClient, GetService, REFIID, void **);

    //
    // IAudioRenderClient
    //

    XFW_HOOKS_TYPEDEF_DECLARATION(IAudioRenderClient, GetBuffer, UINT32, BYTE **);
    XFW_HOOKS_TYPEDEF_DECLARATION(IAudioRenderClient, ReleaseBuffer, UINT32, DWORD);

    XFW_HOOKS_REG_DEFINITION(HookmanagerCoreaudio, IAudioRenderClient, GetBuffer, UINT32, BYTE **);
    XFW_HOOKS_REG_DEFINITION(HookmanagerCoreaudio, IAudioRenderClient, ReleaseBuffer, UINT32, DWORD);

    XFW_HOOKS_FUN_DEFINITION(HookmanagerCoreaudio, IAudioRenderClient, GetBuffer, UINT32, BYTE **);
    XFW_HOOKS_FUN_DEFINITION(HookmanagerCoreaudio, IAudioRenderClient, ReleaseBuffer, UINT32, DWORD);

} // namespace XFW::Native::Hooks
