// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

// minhook
#include <MinHook.h>

// xfw.hooks
#include "xfw_native_hooks.h"



namespace XFW::Native::Hooks {
    void *FunctionAddressFixup(void *address) {
        auto *target = reinterpret_cast<uint8_t *>(address);

        // whoops, we are hooked
        if (target[0] == 0xE9) {
            target = target + 1 + MemoryRead32(reinterpret_cast<size_t>(target + 1)) + 4;

#if defined(_WIN64)
            target -= 0x100000000;
#endif
        }

        return target;
    }


    uint32_t MemoryRead32(size_t address) { return *reinterpret_cast<uint32_t *>(address); }

    uint64_t MemoryRead64(size_t address) { return *reinterpret_cast<uint64_t *>(address); }

    bool HookCreate(void *pTarget, void *pDetour, void **ppOriginal) {
        if(!HookInitialize()){
            return false;
        }
        return MH_CreateHook(pTarget, pDetour, ppOriginal) == MH_OK;
    }

    bool HookRemove(void *pTarget) {
        if(!HookInitialize()){
            return false;
        }
        return MH_RemoveHook(pTarget) == MH_OK;
    }

    bool HookEnable(void *pTarget) {
        if(!HookInitialize()){
            return false;
        }
        return MH_EnableHook(pTarget) == MH_OK;
    }

    bool HookDisable(void *pTarget) {
        if(!HookInitialize()){
            return false;
        }
        return MH_DisableHook(pTarget) == MH_OK;
    }

    bool HookInitialize() {
        auto result = MH_Initialize();
        if (result != MH_OK && result != MH_ERROR_ALREADY_INITIALIZED) {
            return false;
        }
        return true;
    }

} // namespace XFW::Native::Hooks
