// SPDX-License-Identifier: MIT
// Copyright (c) 2014-2021 Rebzzel
// Copyright (c) 2022 Mikhail Paulyshka

// stdlib
#include <memory>

// Windows
#include <wrl.h>

// xfw.hooks
#include "xfw_native_hooks.h"
#include "xfw_native_hooks_d3d.h"


namespace XFW::Native::Hooks {
    //
    // Init
    //

    HookmanagerD3D &HookmanagerD3D::instance() {
        static HookmanagerD3D instance;
        return instance;
    }

    bool HookmanagerD3D::init() {
        if (!_inited) {
            WNDCLASSEXW windowClass{
                .cbSize = sizeof(WNDCLASSEXW),
                .style = CS_HREDRAW | CS_VREDRAW,
                .lpfnWndProc = DefWindowProcW,
                .cbClsExtra = 0U,
                .hInstance = GetModuleHandleW(nullptr),
                .hIcon = nullptr,
                .hCursor = nullptr,
                .hbrBackground = nullptr,
                .lpszMenuName = nullptr,
                .lpszClassName = L"xfw_hook_d3d_wnd_cls",
                .hIconSm = nullptr,
            };
            RegisterClassExW(&windowClass);

            HWND window = ::CreateWindowExW(0U, windowClass.lpszClassName, L"xfw_hook_d3d_wnd_cls", WS_OVERLAPPEDWINDOW, 0, 0, 100, 100,
                                            nullptr, nullptr, windowClass.hInstance, nullptr);

            D3D_FEATURE_LEVEL featureLevel;
            const D3D_FEATURE_LEVEL featureLevels[] = {D3D_FEATURE_LEVEL_10_1, D3D_FEATURE_LEVEL_11_0};

            DXGI_SWAP_CHAIN_DESC swapChainDesc{
                .BufferDesc =
                    {
                        .Width = 100,
                        .Height = 100,
                        .RefreshRate =
                            {
                                .Numerator = 60U,
                                .Denominator = 1U,
                            },
                        .Format = DXGI_FORMAT_R8G8B8A8_UNORM,
                        .ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED,
                        .Scaling = DXGI_MODE_SCALING_UNSPECIFIED,
                    },
                .SampleDesc =
                    {
                        .Count = 1U,
                        .Quality = 0U,
                    },
                .BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT,
                .BufferCount = 1U,
                .OutputWindow = window,
                .Windowed = 1U,
                .SwapEffect = DXGI_SWAP_EFFECT_DISCARD,
                .Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH,
            };

            Microsoft::WRL::ComPtr<IDXGISwapChain> swapChain;
            Microsoft::WRL::ComPtr<ID3D11Device> device;
            Microsoft::WRL::ComPtr<ID3D11DeviceContext> deviceContext;

            bool success = SUCCEEDED(D3D11CreateDeviceAndSwapChain(
                nullptr, D3D_DRIVER_TYPE_NULL, nullptr, 0, featureLevels, std::size(featureLevels), D3D11_SDK_VERSION, &swapChainDesc,
                swapChain.GetAddressOf(), device.GetAddressOf(), &featureLevel, deviceContext.GetAddressOf()));

            if (!success) {
                success = SUCCEEDED(D3D11CreateDeviceAndSwapChain(
                    nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, 0, featureLevels, std::size(featureLevels), D3D11_SDK_VERSION,
                    &swapChainDesc, swapChain.GetAddressOf(), device.GetAddressOf(), &featureLevel, deviceContext.GetAddressOf()));
            }

            if (success) {
                _IDXGISwapChain_vtable.resize(static_cast<size_t>(IDXGISwapChain_methods::Last) + 1);
                std::memcpy(_IDXGISwapChain_vtable.data(), *reinterpret_cast<void **>(swapChain.Get()),
                            _IDXGISwapChain_vtable.size() * sizeof(void *));
                _inited = true;
            }

            ::DestroyWindow(window);
            ::UnregisterClassW(windowClass.lpszClassName, windowClass.hInstance);
        }
        return _inited;
    }

    bool HookmanagerD3D::inited() const { return _inited; }

    //
    // IDXGISwapChain
    //
    XFW_HOOKS_TYPEDEF_DECLARATION(IDXGISwapChain, Present, UINT, UINT);
    XFW_HOOKS_TYPEDEF_DECLARATION(IDXGISwapChain, ResizeBuffers, UINT, UINT, UINT, DXGI_FORMAT, UINT);

    XFW_HOOKS_REG_DEFINITION(HookmanagerD3D, IDXGISwapChain, Present, UINT, UINT);
    XFW_HOOKS_REG_DEFINITION(HookmanagerD3D, IDXGISwapChain, ResizeBuffers, UINT, UINT, UINT, DXGI_FORMAT, UINT);

    XFW_HOOKS_FUN_DEFINITION(HookmanagerD3D, IDXGISwapChain, Present, UINT, UINT);
    XFW_HOOKS_FUN_DEFINITION(HookmanagerD3D, IDXGISwapChain, ResizeBuffers, UINT, UINT, UINT, DXGI_FORMAT, UINT);

} // namespace XFW::Native::Hooks
