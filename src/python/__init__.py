"""
SPDX-License-Identifier: MIT
Copyright (c) 2022 XFW Hooks contributors
"""



#
# Imports
#

#xfw native
from xfw_native.python import unpack_native



#
# Private
#

__xfw_initialized = False



#
# XFW
#

def xfw_is_module_loaded():
    return __xfw_initialized


def xfw_module_init():
    global __xfw_initialized
    if unpack_native('com.modxvm.xfw.native.hooks') is not None:
        __xfw_initialized = True
